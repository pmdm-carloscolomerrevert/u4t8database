package dam.android.carlos.u4t8database.data;

import dam.android.carlos.u4t8database.model.Task;

public final class TodoListDBContract {
    // Commin fields to all DB

    //Database Name
    public  static final String DB_NAME = "TODOLIST.DB";
    // Database Version
    public static  final int DB_VERSION = 1;

    // To prevent someone from accidentally instantiating the contract class
    // mke the constructor private
    private TodoListDBContract(){

    }

    // schema

    // TABLE TASK: Inner class that defines the table Tasks contents
    public static class Tasks {
        // Table name
        public static final String TABLE_NAME = "TASKS";

        // Columns names
        public static final String _ID = "_id";
        public static final String TODO = "todo";
        public static final String TO_ACCOMPLISH = "to_accomplish";
        public static final String DESCRIPTION = "description";
        public static final String PRIORITY = "priority";
        public static final String STATUS = "status";

        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Tasks.TABLE_NAME + " (" + Tasks._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Tasks.TODO + " TEXT NOT NULL, " + Tasks.TO_ACCOMPLISH + " TEXT, " + Tasks.DESCRIPTION + " TEXT," +
                 Tasks.PRIORITY + " INTEGER, " + Tasks.STATUS + " INTEGER );";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Tasks.TABLE_NAME;

        // other table definition would come here

    }

}
