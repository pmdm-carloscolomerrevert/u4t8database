package dam.android.carlos.u4t8database.model;

public class Task {
    private int _id;
    private String todo;
    private String toAccomplish;
    private String description;
    private int priority;
    private int status;


    public Task(int _id, String todo, String toAccomplish, String description, int priority, int status) {
        this._id = _id;
        this.todo = todo;
        this.toAccomplish = toAccomplish;
        this.description = description;
        this.priority = priority;
        this.status = status;
    }

    public int get_id() {
        return _id;
    }


    public String getTodo() {
        return todo;
    }

    public String getToAccomplish() {
        return toAccomplish;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public int getStatus() {
        return status;
    }
}
