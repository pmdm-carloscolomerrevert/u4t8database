package dam.android.carlos.u4t8database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import dam.android.carlos.u4t8database.data.TodoListDBManager;
import dam.android.carlos.u4t8database.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;
    private static Context context;
    private static String[] textProirity, textStatus;


    // Class for each item
    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvSetPriority;
        TextView tvSetStatus;

        public MyViewHolder(View view){
            super(view);

            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvSetPriority = view.findViewById(R.id.tvSetPriority);
            this.tvSetStatus = view.findViewById(R.id.tvSetStatus);
        }

        // sets viewHolder views with data
        public void bind(Task task){
            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
            //TODO 1.2: amb el id del estat i de la prioritat, obtenim el text
            this.tvSetPriority.setText(textProirity[task.getPriority()]);
            this.tvSetStatus.setText(textStatus[task.getStatus()]);
        }

    }

    // constructor: todListDBManager gets DB data
    public MyAdapter(TodoListDBManager todoListDBManager, Context context){
        this.todoListDBManager  = todoListDBManager;
        //TODO 1.1: obtenim el context i l'array de proiritats i de estats
        this.context = context;
        this.textProirity = context.getResources().getStringArray(R.array.priorities);
        this.textStatus = context.getResources().getStringArray(R.array.status);
    }

    // get data from DB
    public void getData(){
        this.myTaskList = todoListDBManager.getTask();
        notifyDataSetChanged();
    }

    // Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        // Create item View
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);

        return new MyViewHolder(itemLayout);
    }

    // replace the data content of a viewHolder (recycles old viewHolder): Layout Manager calls this method


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHolder with data at: position

        viewHolder.bind(myTaskList.get(position));
    }

    // returns the size of dataSet: Layout calls this method
    @Override
    public int getItemCount(){ return myTaskList.size(); }
}
