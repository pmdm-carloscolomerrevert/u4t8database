package dam.android.carlos.u4t8database;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import dam.android.carlos.u4t8database.data.TodoListDBManager;
import dam.android.carlos.u4t8database.model.Task;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;

    // Class for each item
    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;

        public MyViewHolder(View view){
            super(view);

            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
        }

        // sets viewHolder views with data
        public void bind(Task task){
            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
        }

    }

    // constructor: todListDBManager gets DB data
    public MyAdapter(TodoListDBManager todoListDBManager){
        this.todoListDBManager  = todoListDBManager;
    }

    // get data from DB
    public void getData(){
        this.myTaskList = todoListDBManager.getTask();
        notifyDataSetChanged();
    }

    // Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        // Create item View
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);

        return new MyViewHolder(itemLayout);
    }

    // replace the data content of a viewHolder (recycles old viewHolder): Layout Manager calls this method


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHolder with data at: position

        viewHolder.bind(myTaskList.get(position));
    }

    // returns the size of dataSet: Layout calls this method
    @Override
    public int getItemCount(){ return myTaskList.size(); }
}
