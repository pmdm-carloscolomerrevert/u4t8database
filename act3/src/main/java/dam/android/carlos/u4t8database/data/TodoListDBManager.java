package dam.android.carlos.u4t8database.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Currency;

import dam.android.carlos.u4t8database.model.Task;

public class TodoListDBManager {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager(Context context){
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new row
    public void insert(String todo, String when, String description, int priority, int status){
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(TodoListDBContract.Tasks.TODO, todo);
            contentValues.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValues.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValues.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValues.put(TodoListDBContract.Tasks.STATUS, status);

            sqLiteDatabase.insert(TodoListDBContract.Tasks.TABLE_NAME, null, contentValues);
        }
    }

    public void update(int _id, String todo, String when, String description, int priority, int status){
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(TodoListDBContract.Tasks.TODO, todo);
            contentValues.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValues.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValues.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValues.put(TodoListDBContract.Tasks.STATUS, status);

            sqLiteDatabase.update(TodoListDBContract.Tasks.TABLE_NAME, contentValues, TodoListDBContract.Tasks._ID + " = ?", new String[]{ String.valueOf(_id) });

        }

    }

    public void delete (String id){
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
          todoListDBHelper.delete(sqLiteDatabase, id);
        }
    }

    // get all data from Tasks table

    public ArrayList<Task> getTask(){
        ArrayList<Task> taskList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if(sqLiteDatabase != null){
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
            TodoListDBContract.Tasks.TODO,
            TodoListDBContract.Tasks.TO_ACCOMPLISH,
            TodoListDBContract.Tasks.DESCRIPTION,
            TodoListDBContract.Tasks.PRIORITY,
            TodoListDBContract.Tasks.STATUS};

            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null);

            if (cursorTodoList != null){
                // get the cplumn indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()){
                    Task task = new Task(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getInt(priorityIndex),
                            cursorTodoList.getInt(statusIndex));

                    taskList.add(task);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }

        }
        return taskList;
    }

    public ArrayList<Task> getTaskFilter(String id){
        ArrayList<Task> taskList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if(sqLiteDatabase != null){

            Cursor cursorTodoList = sqLiteDatabase.rawQuery(TodoListDBContract.Tasks.SELECT_STATUS + id, null);

            if (cursorTodoList != null){

                // get the cplumn indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);

                // read data and add to ArrayList
                while (cursorTodoList.moveToNext()){
                    Task task = new Task(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getInt(priorityIndex),
                            cursorTodoList.getInt(statusIndex));

                    taskList.add(task);

                    Log.i("loge", task.toString());

                }
                // close cursor to free resources
                cursorTodoList.close();
            }

        }
        return taskList;
    }

    public void deleteTaskCompleted(){
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            todoListDBHelper.deleteCompletedTask(sqLiteDatabase);
        }
    }


    public void deleteAllTask(){
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            todoListDBHelper.deleteAllTask(sqLiteDatabase);
        }
    }

    public void close(){
        todoListDBHelper.close();
    }
}
