package dam.android.carlos.u4t8database;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import dam.android.carlos.u4t8database.data.TodoListDBManager;
import dam.android.carlos.u4t8database.model.Task;

public class editTask extends AppCompatActivity {
    private EditText etTodo;
    private EditText etToAccomplish;
    private EditText etDescription;
    private Spinner spinnerPriority, spinnerStatus;
    private Intent intent;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        setUI();
    }

    private void setUI() {

        etTodo = findViewById(R.id.etTodo);
        etToAccomplish = findViewById(R.id.etToAccomplish);
        etDescription = findViewById(R.id.etDescription);
        spinnerPriority = findViewById(R.id.spinnerPriority);
        spinnerStatus = findViewById(R.id.spinnerStatus);

        ArrayAdapter<CharSequence> spinnerAdapterPriorities = ArrayAdapter.createFromResource(this, R.array.priorities,
                android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(spinnerAdapterPriorities);

        ArrayAdapter<CharSequence> spinnerAdapterStatus = ArrayAdapter.createFromResource(this, R.array.status,
                android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(spinnerAdapterStatus);

        asignarValores();

    }

    private void asignarValores() {
        intent = getIntent();
        task = (Task) intent.getSerializableExtra("task");

        etTodo.setText(task.getTodo());
        etToAccomplish.setText(task.getToAccomplish());
        etDescription.setText(task.getDescription());
        spinnerPriority.setSelection(task.getPriority());
        spinnerStatus.setSelection(task.getStatus());




    }

    public void onClick(View view){

        if(view.getId() == R.id.btnDelete){

            TodoListDBManager todoListDBManager = new TodoListDBManager(this);

            todoListDBManager.delete(String.valueOf(task.get_id()));

            finish();

        }else if (view.getId() == R.id.btnSave){

            TodoListDBManager todoListDBManager = new TodoListDBManager(this);

            todoListDBManager.update(task.get_id(), etTodo.getText().toString(), etToAccomplish.getText().toString(),
                    etDescription.getText().toString(), spinnerPriority.getSelectedItemPosition(), spinnerStatus.getSelectedItemPosition());


            finish();

        }


    }
}
