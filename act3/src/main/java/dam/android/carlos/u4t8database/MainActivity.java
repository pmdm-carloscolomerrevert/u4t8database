package dam.android.carlos.u4t8database;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.os.RecoverySystem;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;

import dam.android.carlos.u4t8database.data.TodoListDBManager;
import dam.android.carlos.u4t8database.model.Task;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //get instance to our BD Manager
        todoListDBManager = new TodoListDBManager(this);
        //TODO 1: cambiem el constructor del MyAdapter per poder fer us del getResources() al adapter
        myAdapter = new MyAdapter(todoListDBManager, this);

        setUI();


    }

    private void setUI() {
        //Action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set feb: opens and activity to ADD A NEW TASK to the DB
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start activity to add a new record to our table
                startActivity(new Intent(getApplicationContext(), AddTaskActivity.class));
            }
        });

        // set recyclerView
        rvTodoList = findViewById(R.id.rvToolList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvTodoList.setAdapter(myAdapter);


        myAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("loge", "Seleccion: " + rvTodoList.getChildLayoutPosition(v));
                task = myAdapter.editTask(rvTodoList.getChildLayoutPosition(v));

                startActivity(new Intent(MainActivity.this, editTask.class).putExtra("task", task));
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

        myAdapter.getData();
    }

    @Override
    protected void onDestroy() {
        // close any connection to DB
        todoListDBManager.close();

        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds item to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
            //TODO 3: Dependiendo de la opción que pulsemos, enviamos la opción al adapter para que carge el
            // arraylist de las tareas que cumplan la característica deseada
        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_search_notStarted:

                myAdapter.getDataFilter(R.id.action_search_notStarted);
                break;

            case R.id.action_search_inProgress:
                myAdapter.getDataFilter(R.id.action_search_inProgress);
                break;

            case R.id.action_search_completed:
                myAdapter.getDataFilter(R.id.action_search_completed);
                break;

            case R.id.action_search_allTasks:
                myAdapter.getData();
                break;

            case R.id.action_delete_allTasks:

                todoListDBManager.deleteAllTask();
                //Realizamos el myAdapter.getData() para
                myAdapter.getData();
                break;

            case R.id.action_delete_completedTasks:

                todoListDBManager.deleteTaskCompleted();
                myAdapter.getData();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
