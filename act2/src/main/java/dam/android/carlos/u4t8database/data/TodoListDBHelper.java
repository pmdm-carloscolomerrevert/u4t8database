package dam.android.carlos.u4t8database.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TodoListDBHelper extends SQLiteOpenHelper {
    // instance to SQLiteOpenHeelper
    private static TodoListDBHelper instanceDBHelper;

    // Use the application context, to not leak Activity context
    public static synchronized TodoListDBHelper getInstance(Context context) {
        //instance must be unique
        if (instanceDBHelper == null){
            instanceDBHelper = new TodoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    // Constructor should be private to prevent direct instantiation
    private TodoListDBHelper(Context context){
        super(context, TodoListDBContract.DB_NAME, null, TodoListDBContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TodoListDBContract.Tasks.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // the upgrade policy is simply discard the table and start over
        sqLiteDatabase.execSQL(TodoListDBContract.Tasks.DELETE_TABLE);
        // create again the DB
        onCreate(sqLiteDatabase);
    }
    //Todo 2: ejecutamos la sentencia que tenemos en el todoListDBContract y le añadimos el id que ha de borrar
    public void delete(SQLiteDatabase sqLiteDatabase, String id){
        sqLiteDatabase.execSQL(TodoListDBContract.Tasks.DELETE_ROW + id);
    }
}
