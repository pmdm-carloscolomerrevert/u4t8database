package dam.android.carlos.u4t8database.model;

import java.io.Serializable;

public class Task implements Serializable {
    private int _id;
    private String todo;
    private String toAccomplish;
    private String description;
    private int priority;
    private int status;


    public Task(int _id, String todo, String toAccomplish, String description, int priority, int status) {
        this._id = _id;
        this.todo = todo;
        this.toAccomplish = toAccomplish;
        this.description = description;
        this.priority = priority;
        this.status = status;
    }

    public int get_id() {
        return _id;
    }


    public String getTodo() {
        return todo;
    }

    public String getToAccomplish() {
        return toAccomplish;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Task{" +
                "_id=" + _id +
                ", todo='" + todo + '\'' +
                ", toAccomplish='" + toAccomplish + '\'' +
                ", description='" + description + '\'' +
                ", priority='" + priority + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
